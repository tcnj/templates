{
  inputs = {
    nixpkgs = {
      url = "github:NixOS/nixpkgs/release-23.11";
    };
    flake-parts = {
      url = "github:hercules-ci/flake-parts";
    };
    devshell = {
      url = "github:numtide/devshell";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    rust-overlay = {
      url = "github:oxalica/rust-overlay";
      inputs.nixpkgs.follows = "nixpkgs";
    };
  };

  outputs = inputs@{ flake-parts, devshell, rust-overlay, ... }:
    flake-parts.lib.mkFlake { inherit inputs; } {
      imports = [
        devshell.flakeModule
      ];

      systems = [
        "x86_64-linux"
      ];

      perSystem = { self', system, pkgs, ... }: {
        _module.args.pkgs = import inputs.nixpkgs {
          inherit system;

          overlays = [
            inputs.rust-overlay.overlays.default
            (_: prev: {
              rustc = prev.rust-bin.stable.latest.default.override {
                extensions = [ "rust-src" ];
              };
            })
            (_: prev: {
              rustPlatform = prev.makeRustPlatform {
                cargo = prev.rustc;
                rustc = prev.rustc;
              };
            })
          ];

          config = { };
        };

        apps.devshell = self'.devShells.default.flakeApp;

        devshells.default = {
          packages = with pkgs; [
            rustc
            gcc
          ];

          env = [
            {
              name = "LD_LIBRARY_PATH";
              eval = "\${PATH:+\${PATH}:}$DEVSHELL_DIR/lib";
            }
            {
              name = "LIBRARY_PATH";
              eval = "\${PATH:+\${PATH}:}$DEVSHELL_DIR/lib";
            }
            {
              name = "PKG_CONFIG_PATH";
              eval = "\${PKG_CONFIG_PATH:+\${PKG_CONFIG_PATH}:}$DEVSHELL_DIR/lib/pkgconfig";
            }
          ];
        };
      };
    }
  ;
}
