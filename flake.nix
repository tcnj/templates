{
  description = "Collection of nix flake templates";

  outputs = { self, nixpkgs }: {

    templates = {
      rust = {
        path = ./templates/rust;
        description = "A simple Rust/Cargo project";
      };
    };

  };
}
